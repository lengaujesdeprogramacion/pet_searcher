import 'package:flutter/material.dart';
import 'package:pet_searcher/views/listViewPets.dart';
import 'package:pet_searcher/views/signIn.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pet Searcher',
      theme: ThemeData(
        
        primarySwatch: Colors.brown,
      ),
      home: SignInView(),
    );
  }
}
