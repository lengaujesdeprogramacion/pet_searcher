class Pet{
  String name;
  String petId;
  String recompensa;
  String descripcion;
  String localizacion;
  String especie;
  String raza;
  String userId;
  String urlFoto;
 

  Pet(String name,String petId, String recompensa,String descripcion,String localizacion, String especie, String raza, String userId, String urlFoto){
    this.name=name;
    this.petId= petId;
    this.recompensa=recompensa;
    this.descripcion=descripcion;
    this.localizacion=localizacion;
    this.especie=especie;
    this.raza=raza;
    this.urlFoto=urlFoto;
    this.userId=userId;
  }
  Pet.insertar(this.name,this.especie,this.descripcion,this.raza,this.localizacion,this.urlFoto,this.userId,this.recompensa);

  String get getName => name;

  String get categoria => null;

  get titulo => null;

  set setName(String name) => this.name = name;

  String get getPetId => petId;

  set setPetId(String petId) => this.petId = petId;

  String get getRecompensa => recompensa;

  set setRecompensa(String recompensa) => this.recompensa = recompensa;

  String get getDescripcion => descripcion;

  set setDescripcion(String descripcion) => this.descripcion = descripcion;

  String get getLocalizacion => localizacion;

  set setLocalizacion(String localizacion) => this.localizacion = localizacion;

  String get getEspecie => especie;

  set setEspecie(String especie) => this.especie = especie;

  String get getRaza => raza;

  set setRaza(String raza) => this.raza = raza;

  String get getUserId => userId;

  set setUserId(String userId) => this.userId = userId;
  
  String get getUrlFoto => urlFoto;

  set setUrlFoto(String urlFoto) => this.urlFoto = urlFoto;
  void printPet(){
    print(name+urlFoto);
  }
}