import 'package:flutter/material.dart';

class User{
  String userId;
  String nombre;
  String apellido;
  String telefono;
  String city;
  String country;
  String usuario;
  String contrasena;
  String urlFoto;
  
  
  User(String userId,String nombre,String apellido,String telefono,String city,String country,String usuario,String contrasena,String urlFoto){
    this.userId=userId;
    this.nombre=nombre;
    this.apellido=apellido;
    this.telefono=telefono;
    this.city=city;
    this.country=country;
    this.usuario=usuario;
    this.contrasena=contrasena;
    this.urlFoto=urlFoto;
  }
  User.insertar(this.nombre,this.apellido,this.telefono,this.city,this.country,this.usuario,this.contrasena);

  List<Widget> mostrarUsuario(){
    var result=List<Widget>();
    Widget foto;
    if(this.urlFoto==null){
      foto=_mostrarFotoDefault(500);
      result.add(foto);
      result.add(Container(
      padding: EdgeInsets.fromLTRB(50, 100, 50, 100),
      child: Text("No hay usuario registrado")
      
      ));
    }
    else{
      foto=_mostrarFoto(500);
      result.add(foto);
      result.add(Padding(
        padding: EdgeInsets.symmetric(vertical: 30),
        child: Text("${this.nombre} ${this.apellido}",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
        
        ));
      result.add(Padding(
        padding: EdgeInsets.symmetric(vertical: 30),
        child: Row(
          children: <Widget>[
            Text("Usuario: ",
            style: TextStyle(
              fontSize:20,
              fontWeight: FontWeight.bold,
            ),
            ),
            Text(this.usuario,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.normal,
            ),
            )
          ],
        )
        
        ));
    }
    return result;
  }
  Widget _mostrarFoto(double height){
    return CircleAvatar(
      radius: 200,
      backgroundImage: NetworkImage(urlFoto),
    );
  }
  Widget _mostrarFotoDefault(double height){
    return CircleAvatar(
      radius: 60,
      backgroundImage: NetworkImage(
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_9bof4bVKtjrRlgEP5w1OBTTUrY3ygKDuL8geeGcki2Pv1dAx&s") ,
    );
  }


}