
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pet_searcher/obj/anuncio.dart';
import 'package:pet_searcher/obj/pet.dart';
import 'package:pet_searcher/obj/user.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:pet_searcher/views/anuncioView.dart';
import 'package:pet_searcher/views/insertAnuncioView.dart';
import 'package:pet_searcher/views/petInsertView.dart';
import 'package:pet_searcher/views/petView.dart';
import 'package:pet_searcher/views/signIn.dart';
import 'package:pet_searcher/widgets/viewWidgets.dart';

class ListViewPets extends StatefulWidget {
  final User user;
  BuildContext context;
  ListViewPets(this.user,this.context) ;

  @override
  _ListViewPetsState createState() => _ListViewPetsState(user,context);
}

class _ListViewPetsState extends State<ListViewPets> {
  Function floatAction= (){};
  Widget body=petsBody();
   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
   final User user;
   final BuildContext context;
   _ListViewPetsState(this.user,this.context);

 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: _crearDrawer(context),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu,
          color: Colors.white,),
          onPressed: (){
            _scaffoldKey.currentState.openDrawer();
          },
        ),
        
        title: ViewWidget.logoPetSearcher(),
      ),
      body: body,
      floatingActionButton: FloatingActionButton(
        onPressed: floatAction,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 0,
        items: _listaBottoms(),
        onTap: _onItemTap,
      ),
        
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
  void _onItemTap(int index){
    if(index==0){
      setState(() {
        floatAction=petsFloat;
        body=petsBody();
      });
    }
    if(index==1){
      floatAction=anunciosFloat;
      setState(() {
        body=AnuncioView(context,user);
      });
    }
  }
  List<BottomNavigationBarItem> _listaBottoms(){
    return const<BottomNavigationBarItem> [ 
        BottomNavigationBarItem(
          icon: Icon(Icons.pets,
          color: Colors.brown,
          ),
          title: Text('Pets')
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.new_releases,
          color: Colors.brown,),
          title: Text('Anuncios'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.search,
          color: Colors.brown,),
          title: Text("Buscar")
        ),
        ];
  }

  static Future<List<Pet>> _getPets() async{

    var petsData= await http.get("http://127.0.0.1:8080/pets");
 
    var jsonPets= json.decode(petsData.body);
    List<Pet> pets=[];
    for(var petJson in jsonPets){
      Pet pet=Pet(petJson["nombre"],petJson["pet_id"],petJson["recompensa"],petJson["descripcion"],petJson["localizacion"],
      petJson["especie"],petJson["raza"],petJson["user_id"],petJson["foto"]);
      pets.add(pet);
    }
    return pets;
  }
  
  static Widget _cargando(){
    return Container(
      child: Center(      
          child: 
            Icon(Icons.cloud_upload,
                color: Colors.brown
            )
          
        ),
      );
    
  }

  static Widget _listaPets(BuildContext context,AsyncSnapshot snapshot){
    if(snapshot.data==null){
      return _cargando();
    }
    else{
    return ListView.builder(
      itemCount: snapshot.data.length,
      itemBuilder: (BuildContext context,int index){
        return PetView(snapshot.data[index]);
      } 
    );
    }
  }
  Widget _crearDrawer(BuildContext context){
    return Drawer(
        child: ListView(
          
          children: 
            _panelDrawer(context)
          
        ),
      );
  }
  List<Widget> _panelDrawer(BuildContext context){
    var result= user.mostrarUsuario();
    result.add(
      Container(
      child:ViewWidget.botonWidget("Cerrar Sesion", _tapIniciarSesion,5),
      padding: EdgeInsets.fromLTRB(50, 0, 50, 0),
      )
    );
    result.add(SizedBox(height: 30,));
    
    return result;
  }
  void _tapIniciarSesion(){
    Route route = MaterialPageRoute(builder: (context) => SignInView());
    Navigator.push(context,route);
  }
  static Widget  petsBody(){
    return Container(
        child: FutureBuilder(
          future: _getPets(),
          builder: _listaPets,

        ),
      );
  }
  void petsFloat(){
    Route route= MaterialPageRoute(builder: (context)=>PetInsertView(context,user.userId));
    Navigator.push(context,route);       
  }
  void anunciosFloat(){
    Route route= MaterialPageRoute(builder: (context)=>InsertAnuncioView(context,user.userId));
    Navigator.push(context,route);       
  }
}
