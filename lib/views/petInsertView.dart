import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:pet_searcher/obj/pet.dart';
import 'package:pet_searcher/widgets/viewWidgets.dart';

class PetInsertView extends StatelessWidget{
  BuildContext context;
  String userId;
  PetInsertView(this.context,this.userId);
  final TextEditingController nombreController=TextEditingController();
  final TextEditingController razaController=TextEditingController();
  final TextEditingController especieController=TextEditingController();
  final TextEditingController descripcionController=TextEditingController();
  final TextEditingController recompensaController=TextEditingController();
  final TextEditingController fotoUrlController=TextEditingController();
  final TextEditingController localizacionController=TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(15.0, 30.0, 0.0, 0.0),
                child: Text(
                  'INSERT PET ',
                  style:
                    TextStyle(fontSize: 80.0, fontWeight: FontWeight.bold),
                  ),
                ),
              ]
            ),
          ),
          Container( 
          padding: EdgeInsets.fromLTRB(10, 0, 30, 0),
          child: Column(
            children: <Widget>[
                        ViewWidget.textInput("NOMBRE",nombreController),
                        SizedBox(height: 8.0),
                        ViewWidget.textInput("ESPECIE",especieController),                 
                        SizedBox(height: 8.0),
                        ViewWidget.textInput("RAZA",razaController),
                        SizedBox(height: 8.0),
                        ViewWidget.textInput("DESCRIPCION",descripcionController),                 
                        SizedBox(height: 8.0),
                        ViewWidget.textInput("RECOMPENSA",recompensaController),
                        SizedBox(height: 8.0),
                        ViewWidget.textInput("URL DE FOTO",fotoUrlController),                 
                        SizedBox(height: 8.0),
                        ViewWidget.textInput("LOCALIZACION",localizacionController),            
                        SizedBox(height: 20.0),
                        ViewWidget.botonWidget("Publicar mascota perdida", _publicarMascota,200),
                        SizedBox(height: 30.0),
                        ViewWidget.botonWidget("Ir atras.", _salir,200),
              ],
            ),
          ),
        ], 
      ),
    );
  }
  void _publicarMascota() async{
    Pet pet=Pet.insertar(nombreController.text,especieController.text,descripcionController.text,
    razaController.text,localizacionController.text,fotoUrlController.text,userId,recompensaController.text);
    String url = 'http://127.0.0.1:8080/pets/post';
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{"nombre": "${pet.name}", "especie": "${pet.especie}", "descripcion":"${pet.descripcion}", "raza":"${pet.raza}", "localizacion": "${pet.localizacion}", "foto":"${pet.urlFoto}", "user_id":"${userId}", "recompensa":"${pet.recompensa}"}';
    
    // make POST request
    Response response = await post(url, headers: headers, body: json);
    // check the status code for the result
    int statusCode = response.statusCode;
    // this API passes back the id of the new item added to the body
    if(statusCode==404){
      ViewWidget.showErrorDialog(context, "Error de conexion", "No hay conexion con el servidor");
      return null;
    }
    Navigator.pop(context);
  }
  void _salir(){
    Navigator.pop(context);
  }


}