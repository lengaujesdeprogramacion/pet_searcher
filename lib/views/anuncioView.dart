import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pet_searcher/obj/anuncio.dart';
import 'package:pet_searcher/obj/user.dart';
import 'package:http/http.dart' as http;
import 'package:pet_searcher/views/itemAnuncio.dart';



class AnuncioView extends StatefulWidget {
  BuildContext context;
  User user;

  AnuncioView(this.context, this.user);

  @override
  _AnuncioState createState() => _AnuncioState(context,user);
}




class _AnuncioState extends State<AnuncioView> {
  BuildContext context;
  User user;

  _AnuncioState(this.context, this.user);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: FutureBuilder(
        future: _getAnuncios(),
        builder: _listaAnuncios,

      ),
    );
  }

  Future<List<Anuncio>> _getAnuncios() async {
    var anunciosData= await http.get("http://127.0.0.1:8080/anuncios");
 
    var jsonAnuncios= json.decode(anunciosData.body);
    List<Anuncio> anuncios=[];
    for(var anuncioJson in jsonAnuncios){
      Anuncio anuncio=Anuncio(anuncioJson["anuncio_id"],anuncioJson["titulo"],anuncioJson["descripcion"],anuncioJson["categoria"],anuncioJson["user_id"]);
      anuncios.add(anuncio);
    }
    return anuncios;
  }

    Widget _cargando(){
      return Container(
        child: Center(      
            child: 
              Icon(Icons.cloud_upload,
                  color: Colors.brown
              )
            
          ),
        );
      
  }

  Widget _listaAnuncios(BuildContext context,AsyncSnapshot snapshot){
    if(snapshot.data==null){
      return _cargando();
    }
    else{
    return ListView.builder(
      itemCount: snapshot.data.length,
      itemBuilder: (BuildContext context,int index){
        return ItemAnuncio(snapshot.data[index]);
      } 
    );
    }

  }
}