import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pet_searcher/obj/anuncio.dart';

import 'package:pet_searcher/obj/user.dart';
import 'package:pet_searcher/views/userInfoView.dart';
import 'package:pet_searcher/widgets/viewWidgets.dart';
import 'package:http/http.dart' as http;

class ItemAnuncio extends StatefulWidget{
  final Anuncio anuncio;
  ItemAnuncio(this.anuncio);

 
    

  @override
  State<StatefulWidget> createState() {
    
    return _ItemAnuncioState(anuncio);
  }

}

class _ItemAnuncioState extends State<ItemAnuncio>{
  Anuncio anuncio;
  _ItemAnuncioState (this.anuncio);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: _obtenerUser(),
        builder: _listTileAnuncio,
      ),
    );
  }
  Widget _listTileAnuncio(BuildContext context, AsyncSnapshot snapshot){
    if(snapshot.data == null){
      return Container(
        child: Center(
          child: Icon(Icons.cloud_upload),
        ),
      );
    }
    else{
      return Column(
        children: <Widget>[
          Card(
                child: Column(
                children: <Widget>[
                  _encabezado(snapshot.data),
                  _body(snapshot.data),
                ],
              ),
            
          ),
          SizedBox(height: 30,)
        ],
      );
    }
  }
  Row _encabezado(User user){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
         FlatButton(
                        onPressed: (){
                          Route route = MaterialPageRoute(builder: (context)=>UserInfoView(user,context));
                          Navigator.push(context,route);
                        },
                        child:CircleAvatar(
                
                               backgroundImage:NetworkImage(user.urlFoto),
                               ),
              ),
           
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
               
              children: <Widget>[
                
                 FlatButton(
                            child: Text("${user.nombre} ${user.apellido}",
                                      style:TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold)
                          ),
                          onPressed: (){
                            Route route = MaterialPageRoute(builder: (context)=>UserInfoView(user,context));
                            Navigator.push(context,route);
                          },
                          ),
                
               Text(anuncio.categoria,
                  style:TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.normal),
                ), 
              ],
            ),
        SizedBox(width: 30,),
        
      ],

                        
    );
  }
  Container _body(User user){
    return Container(
      padding: EdgeInsets.fromLTRB(20, 5, 5, 0),
      child: 
        _columnaBody("${anuncio.titulo} ", anuncio.descripcion),

      );
  }

  Column _columnaBody(String encabezado, String cuerpo){
    return Column(
        
        children: <Widget>[
          Text(encabezado,
              style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              ),
          ),
          SizedBox(height: 10,),
          Text(cuerpo,
              style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.normal,
              ),
          ),
        ],
       
    );
  }
  
  
  Future<User> _obtenerUser() async{
    User user;
    try{
    String userid=anuncio.user_id;
    var userData=await http.get("http://127.0.0.1:8080/users/${userid}");
    var userJson= json.decode(userData.body);
    user=User(userJson["user_id"],userJson["first_name"],userJson["last_name"],
    userJson["phone_number"],userJson["city"],userJson["country"],userJson["usuario"],
    userJson["contrasena"],userJson["foto"]);
    
    }
    catch(e){
      ViewWidget.showErrorDialog(context, "Error", "No se ha encontrado usuario");
    }
    return user;
  }
  }

