import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pet_searcher/obj/pet.dart';
import 'package:pet_searcher/obj/user.dart';
import 'package:pet_searcher/views/userInfoView.dart';
import 'package:pet_searcher/widgets/viewWidgets.dart';
import 'package:http/http.dart' as http;

class PetView extends StatefulWidget{
  final Pet pet;
  PetView(this.pet);

 
    

  @override
  State<StatefulWidget> createState() {
    
    return _PetViewState(pet);
  }

}

class _PetViewState extends State<PetView>{
  Pet pet;
  _PetViewState(this.pet);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: _obtenerUser(),
        builder: _listTilePet,
      ),
    );
  }
  Widget _listTilePet(BuildContext context, AsyncSnapshot snapshot){
    if(snapshot.data == null){
      return Container(
        child: Center(
          child: Icon(Icons.cloud_upload),
        ),
      );
    }
    else{
      return Column(
        children: <Widget>[
          Card(
                child: Column(
                children: <Widget>[
                  _encabezado(snapshot.data),
                  _imagen(),
                  _body(snapshot.data),
                ],
              ),
            
          ),
          SizedBox(height: 30,)
        ],
      );
    }
  }
  Row _encabezado(User user){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
         FlatButton(
                        onPressed: (){
                          Route route = MaterialPageRoute(builder: (context)=>UserInfoView(user,context));
                          Navigator.push(context,route);
                        },
                        child:CircleAvatar(
                
                               backgroundImage:NetworkImage(user.urlFoto),
                               ),
              ),
           
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
               
              children: <Widget>[
                
                 FlatButton(
                            child: Text("${user.nombre} ${user.apellido}",
                                      style:TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold)
                          ),
                          onPressed: (){
                            Route route = MaterialPageRoute(builder: (context)=>UserInfoView(user,context));
                            Navigator.push(context,route);
                          },
                          ),
                
               Text(pet.localizacion,
                  style:TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.normal),
                ), 
              ],
            ),
        SizedBox(width: 30,),
         FlatButton(
                            onPressed: (){
                              Route route = MaterialPageRoute(builder: (context)=>UserInfoView(user,context));
                              Navigator.push(context,route);
                            },
                            child:  Row(children: <Widget>[
                                      Text("\$ ",
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.green,
                                          fontWeight: FontWeight.bold,
                                        )
                                      ),
                                      Text(pet.recompensa.toString(),
                                          style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          ),
                                      ),
                                          ],
                                    ),
                            
              ),
        
      ],

                        
    );
  }
  Column _body(User user){
    return Column(
      children: <Widget>[
        _filaBody("${pet.name} ", pet.descripcion),
        _filaBody("Raza ", pet.raza),
      ]
        

      );
  }

  Container _filaBody(String encabezado, String cuerpo){
    return Container(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Row(
        
        children: <Widget>[
          Text(encabezado,
              style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              ),
          ),
          Text(cuerpo,
              style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.normal,
              ),
          ),
        ],
        
      ),
    );
  }

  Widget _imagen(){
    return Container(
      height: MediaQuery.of(context).size.height / 3,
      width: MediaQuery.of(context).size.width,
      child: Image.network(
        pet.urlFoto,
        fit:BoxFit.fill),
    );
  }
  
  
  
  Future<User> _obtenerUser() async{
    User user;
    try{
    String userid=pet.userId;
    var userData=await http.get("http://127.0.0.1:8080/users/${userid}");
    var userJson= json.decode(userData.body);
    user=User(userJson["user_id"],userJson["first_name"],userJson["last_name"],
    userJson["phone_number"],userJson["city"],userJson["country"],userJson["usuario"],
    userJson["contrasena"],userJson["foto"]);
    
    }
    catch(e){
      ViewWidget.showErrorDialog(context, "Error", "No se ha encontrado usuario");
    }
    return user;
  }
  }

