package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func InicializarRouter() *mux.Router {
	r := mux.NewRouter()
	//Pets
	r.HandleFunc("/pets", HandlerGetMascotas).Methods("GET")
	r.HandleFunc("/pets/{pet_id}", HandlerGetMascota).Methods("GET")
	r.HandleFunc("/pets/post", HandlerCreateMascota).Methods("POST")
	r.HandleFunc("/pets/{pet_id}", HandlerDeleteMascota).Methods("DELETE")
	//Usuarios
	r.HandleFunc("/users/{usuario}/{contrasena}", HandlerGetUser).Methods("GET")
	r.HandleFunc("/users/{first_name}/{last_name}/{phone_number}/{city}/{country}/{usuario}/{contrasena}", HandlerCreateUser).Methods("PUT")
	r.HandleFunc("/users", HandlerGetUsers).Methods("GET")
	r.HandleFunc("/users/{user_id}", HandlerGetUserById).Methods("GET")
	r.HandleFunc("/users/post", HandlerCreateUser).Methods("POST")

	//Anuncios
	r.HandleFunc("/anuncios", HandlerGetAnuncios).Methods("GET")
	r.HandleFunc("/anuncios/{anuncio_id}", HandlerGetAnuncio).Methods("GET")
	r.HandleFunc("/anuncios/categoria/{category}", HandlerGetAnuncioCategoria).Methods("GET")
	r.HandleFunc("/anuncios/post", HandlerCreateAnuncio).Methods("POST")
	return r
}

func HandlerGetMascotas(w http.ResponseWriter, r *http.Request) {
	pets, err := store.GetPets()
	petsListBytes, err := json.Marshal(pets)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(petsListBytes)

}

func HandlerGetMascota(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	petTmp := &Pet{Pet_id: vars["pet_id"]}
	pet, err := store.GetPet(petTmp)
	petBytes, err := json.Marshal(pet)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(petBytes)

}
func HandlerCreateMascota(w http.ResponseWriter, r *http.Request) {

	pet := Pet{}
	err := json.NewDecoder(r.Body).Decode(&pet)
	if err != nil {
		http.Error(w, "Error reading request body",
			http.StatusInternalServerError)
	}
	err = store.CreatePet(&pet)
	if err != nil {
		print("Error al insertar a base de datos")
	}

}

func HandlerDeleteMascota(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	petTmp := &Pet{Especie: vars["pet_id"]}
	err := store.DeletePet(petTmp)
	if err != nil {
		fmt.Print(err)
	}
}

func HandlerGetUsers(w http.ResponseWriter, r *http.Request) {
	users, err := store.GetUsers()

	usersListBytes, err := json.Marshal(users)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(usersListBytes)

}

func HandlerGetUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userTmp := &User{Usuario: vars["usuario"], Contrasena: vars["contrasena"]}
	user, err := store.GetUser(userTmp)
	userBytes, err := json.Marshal(user)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(userBytes)

}

func HandlerGetUserById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userTmp := &User{User_id: vars["user_id"]}
	user, err := store.GetUserById(userTmp)
	userBytes, err := json.Marshal(user)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(userBytes)
}

func HandlerGetAnuncios(w http.ResponseWriter, r *http.Request) {
	anuncios, err := store.GetAnuncios()
	anunciosListBytes, err := json.Marshal(anuncios)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(anunciosListBytes)
}
func HandlerGetAnuncio(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	anuncioTmp := &Anuncio{Anuncio_id: vars["anuncio_id"]}
	anuncio, err := store.GetAnuncio(anuncioTmp)
	anuncioBytes, err := json.Marshal(anuncio)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(anuncioBytes)

}

func HandlerGetAnuncioCategoria(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	anuncioTmp := &Anuncio{Categoria: vars["category"]}
	anuncio, err := store.GetAnuncioCategoria(anuncioTmp)
	anuncioBytes, err := json.Marshal(anuncio)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(anuncioBytes)

}

func HandlerCreateAnuncio(w http.ResponseWriter, r *http.Request) {

	anuncio := Anuncio{}
	err := json.NewDecoder(r.Body).Decode(&anuncio)
	if err != nil {
		http.Error(w, "Error reading request body",
			http.StatusInternalServerError)
	}
	err = store.CreateAnuncio(&anuncio)
	if err != nil {
		print("Error al insertar a base de datos")
	}

}

func HandlerCreateUser(w http.ResponseWriter, r *http.Request) {

	user := User{}
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, "Error reading request body",
			http.StatusInternalServerError)
	}
	err = store.CreateUser(&user)
	if err != nil {
		print("Error al insertar a base de datos")
	}

}
