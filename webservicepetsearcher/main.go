package main

import (
	"net/http"
)

func main() {
	InicializarBD()
	r := InicializarRouter()
	http.ListenAndServe(":8080", r)
}
